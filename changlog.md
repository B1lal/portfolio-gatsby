_Changelog to track the updates in the pages and trigger rebuild on netlify through gitlab_

17-2-2020: Fixed typo on resume page, added twitter bot project, added content to one blog entry
8-03-2020: Added a blog post about web/mobile development process.
19-04-2020: Added job update.
05-07-2020: Improved current job description, updated technologies learnt.
23-07-2020: Added netlify config file to use npm, minor updates to portfolio.
02-08-2020: Fixed the google font issue cuasing deployment trouble.
03-02-2021: Added 2020 Review Blog and google analaytics
15-03-2021: Updated current job and info, footer.
