import React from 'react'
import {Link} from 'gatsby'


import headerStyles from './header.module.scss';

const Header = () => {
    return(
        <header className={headerStyles.header}>
            <nav>
                <ul className={headerStyles.navlist}>
                    <li >
                        <Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to='/'>/home</Link>
                    </li>
                    <li>
                        <Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to='/resume'>/resume</Link>
                    </li>
                    <li>
                        <Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to='/projects'>/projects</Link>
                    </li>
                    <li>
                        <Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to='/blog'>/blog</Link>
                    </li>
                    <li>
                        <Link className={headerStyles.navItem} activeClassName={headerStyles.activeNavItem} to='/contact'>/contact</Link>
                    </li>
                </ul>
            </nav>
        </header>
    )
}


export default Header;