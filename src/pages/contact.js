import React from "react"

import Layout from '../components/layout'
import Head from '../components/head'

import gitlab from '../images/gitlab.png';
import insta from '../images/insta.png';
import LinkedIn from '../images/linkedin.png'
import twitter from '../images/twitter.png'


import contactStyles from './contact.module.scss'
import Emoji from "a11y-react-emoji";

const ContactPage = () => {

    return (
        <Layout>
            <Head title="Contact" />
            <div className={contactStyles.container}>
                <div className={contactStyles.contactMain}>
                    <h1>Get in touch with me via</h1>
                </div>
                <div className={contactStyles.contactSub}>
                    <p>email:= saybilalshah@gmail.com</p>
                    <p>cell phone:= +60 11 2812 4585</p>
                </div>
                <div className={contactStyles.boxes}>
                    <div className={contactStyles.box}>
                        <a href="https://www.linkedin.com/in/b1lalshah/" target="_blank" rel="noopener noreferrer"><img alt="LinkedIn" src={LinkedIn} /></a>
                    </div>
                    <div className={contactStyles.box}>
                        <a href="https://gitlab.com/B1lal" target="_blank" rel="noopener noreferrer"><img alt="GitLab" src={gitlab} /></a>
                    </div>
                    <div className={contactStyles.box}>
                        <a href="https://twitter.com/10lalShah" target="_blank" rel="noopener noreferrer"><img alt="Twitter" src={twitter} /></a>
                    </div>
                    <div className={contactStyles.box}>
                        <a href="https://instagram.com/bilal.shah97/" target="_blank" rel="noopener noreferrer"><img alt="Instagram" src={insta} /></a>
                    </div>
                </div>
                <p>Hmu <Emoji symbol="🤙" label="hmu"/><Emoji symbol="🤙" label="hmu"/><Emoji symbol="🤙" label="hmu"/></p>
            </div>
        </Layout>
    )
}

export default ContactPage;