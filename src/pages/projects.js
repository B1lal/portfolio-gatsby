import React from "react"
import { graphql } from 'gatsby'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'

import Layout from '../components/layout'
import Head from '../components/head'

import gitlab from '../images/gitlab.png'

import projectStyles from './projects.module.scss'

export const query = graphql`
    query {
    allContentfulProjects {
        edges {
        node {
            projectName
            projectLink
            projectDescription {
            json
            }
            projectTechnologies
        }
        }
    }
    }
`


const ProjectsPage = (props) => {

    return (
        <Layout>
            <Head title="Projects" />
            <h1 className={projectStyles.title}>Projects</h1>
            <ol className={projectStyles.list}>
                {props.data.allContentfulProjects.edges.map((edge) => {
                    return (
                        <li className={projectStyles.projectList}>
                            <div className={projectStyles.projectLink}>
                                <a href={edge.node.projectLink} target="_blank" rel="noopener noreferrer">
                                    <img src={gitlab} alt="project-link" />
                                </a>
                            </div>
                            <div className={projectStyles.projectContent}>
                                <h3>Title: {edge.node.projectName}</h3>
                                <p>Description: {documentToReactComponents(edge.node.projectDescription.json)}</p>
                                <p>Technologies: {edge.node.projectTechnologies}</p>
                            </div>
                        </li>
                    )
                })}
            </ol>
        </Layout>
    )
}

export default ProjectsPage;