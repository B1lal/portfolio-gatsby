import React from "react"
import Emoji from 'a11y-react-emoji'

import Layout from '../components/layout'
import Head from '../components/head'

import mainStyles from './index.module.scss';

const IndexPage = () => {

    return (
        <Layout>
            <Head title="Home" />
            <div className={mainStyles.container}>
                <div className={mainStyles.mainContent}>
                    <div className={mainStyles.title}>
                        <h1><Emoji symbol="👋" label="wave" /> Hi, I'm Bilal Shah</h1>
                        <p>I’m a computer engineer, I like to code mobile and web applications, tinker around with electronics and work on cool technology.
                         Check out my projects and what I’m up to these days.</p>

                    </div>

                    <div className={mainStyles.boxes}>
                        <div className={mainStyles.box}>
                            <h3>Based In</h3>
                            <h3>--------</h3>
                            <p>Kuala Lumpur</p>
                        </div>
                        <div className={mainStyles.box}>
                            <h3>Currently</h3>
                            <h3>--------</h3>
                            <a href="https://socar.my/" target="_blank" rel="noopener noreferrer">
                                <p id="link">Working at SOCAR <Emoji symbol="🚙" label="car" /></p>
                            </a>
                        </div>
                        <div className={mainStyles.box}>
                            <h3>Representing</h3>
                            <h3>--------</h3>
                            <p>Pakistan <Emoji symbol="🇵🇰" label="pakistan-flag" /></p> <span role='img'></span>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default IndexPage;

