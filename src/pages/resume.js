import React from "react"

import Layout from '../components/layout'
import Head from '../components/head'
import Emoji from 'a11y-react-emoji'


import resumeStyles from './resume.module.scss';

const ResumePage = () => {

    return (
        <Layout>
            <Head title="Resume" />
            <div className={resumeStyles.container}>
                <div className={resumeStyles.education}>
                    <h3>Education <Emoji symbol="🎓" label="university" /></h3>
                    <div className={resumeStyles.school}>
                        <p className={resumeStyles.underline}>Bachelor in Computer Information & Electronics Engineering</p>
                        <p className={resumeStyles.underline}>Minor in Business Administration</p>
                        <p>International Islamic University Malaysia</p>
                        <p>2015 - 2019</p>
                        <p>Relevant Coursework: Programming for Engineers,
                        Digital Logic Design, Computer Organization and
                        Microprocessors, Data Structures and Algorithm
                        Design, Computer Architecture and Systems Design,
                        Software Engineering Design, Multimedia Information
                        Systems, Computer Networking, Database Design,
            Object Oriented Programming with Java, Operating Systems</p>
                    </div>

                    <br></br>
                    <div className={resumeStyles.school}>
                        <p className={resumeStyles.underline}>American High School Diploma</p>
                        <p>International School of Kuantan</p>
                        <p>2011 - 2015</p>
                        <p>Class Valedictorian</p>
                    </div>

                </div>

                <div className={resumeStyles.workExperience}>
                    <h3>Work Experience <Emoji symbol="💼" label="work" /></h3>
                    <div className={resumeStyles.company}>
                        <p className={resumeStyles.underline}>SOCAR Mobility Malaysia sdn bhd</p>
                        <p>React Native Malaysia</p>
                        <p>Mobile application developer using React Native to develop and maintain the main business applications.
                            Tasks include integrating native and javascript third party libraries, creating custom libraries for 
                            curated user experiences. Continuously working on improving the codebase with optimizations and best coding practices. 
            </p>
                        <p>March 2021 -  Present</p>
                    </div>
                    <div className={resumeStyles.company}>
                        <p className={resumeStyles.underline}>tiger lab sdn bhd</p>
                        <p>Junior Frontend Web Developer </p>
                        <p>Frontend developer working in insurance-tech industry to delivery enterprise level solutions
                        to insurance companies. Working with technologies such Angular, React and Django to 
                        deliver high quality UI/UX experiences.
            </p>
                        <p>Apr 2020 -  Feb 2021</p>
                    </div>


                    <div className={resumeStyles.company}>
                        <p className={resumeStyles.underline}>REKA Inisiatif Sdn Bhd</p>
                        <p>Software Developer (Intern)</p>
                        <p>Part of the core software development team, relied upon to deliver to clients. Worked on MERN stack to develop PWAs using React and Firebase.
                        NodeJs and MongoDB used separately for projects tasked to create own secure database. Worked in a self motivated team to quickly deliver products.
            </p>
                        <p>Oct 2019 - Dec 2019</p>
                    </div>

                    <div className={resumeStyles.company}>
                        <p className={resumeStyles.underline}>Maze Design Lab</p>
                        <p>Software Engineer</p>
                        <p>Part of the core team, tasked to provide technical solutions to business. Working with a team to develop web and native mobile applications for clients and internal products.
                </p>
                        <p>Jan 2019 - Present </p>
                    </div>

                    <div className={resumeStyles.company}>
                        <p className={resumeStyles.underline}>Pakistan Space and Upper Atmosphere Research Commission</p>
                        <p>Software Engineer (Intern)</p>
                        <p>Part of the GIS department, trained to use software suites such as ArcGIS and QGIS to develop and maintain databases using SQL language.
                        Worked on Android development project to create database of hospitals to quickly send location of the hospital based on users current location.
            </p>
                        <p>June 2018 - August 2018 </p>
                    </div>

                    <div className={resumeStyles.programming}>
                        <h3>Programming <Emoji symbol="💻" label="programming" /></h3>
                        <p className={resumeStyles.underline}>Languages:</p>
                        <p>Java Script, Python, C/C++, Java, MySQL</p>
                        <p className={resumeStyles.underline}>Technologies and Frameworks:</p>
                        <p>React, React Native, NodeJS, MongoDB, Firebase, Angular, Django, Git, Docker, GatsbyJS </p>
                        <p className={resumeStyles.underline}>Operating Systems:</p>
                        <p>Mac OSX, Windows, Ubuntu(Linux)</p>
                    </div>
                </div>

                <div className={resumeStyles.volunteer}>
                    <h3>Volunteer Work <Emoji symbol="🙋‍♂️" label="volunterr" /></h3>
                    <div className={resumeStyles.event}>
                        <p className={resumeStyles.underline}>Vice President of Students Industrial Training Club, IIUM</p>
                        <p>Part of the main board of the society. Responsible
                        for organizing site visits for students to increase
                their exposure to real-world technical problems. </p>
                        <p>Nov 17 - Mar 19</p>
                    </div>

                    <div className={resumeStyles.event}>
                        <p className={resumeStyles.underline}>Multimedia Team Member for Global Ummatic Festival 2017</p>
                        <p>Member of the team responsible for handling audio/visual effects for the duration of the event.
                Responsible  for creating posters, graphics and name-tags for the activities that were part of the week long festival.</p>
                        <p>Nov 17</p>
                    </div>

                    <div className={resumeStyles.event}>
                        <p className={resumeStyles.underline}>IEEE Student Member</p>
                        <p>Student member of the IEEE IIUM chapter. Responsibilities included organizing seminars and promoting IEEE between the students.</p>
                        <p>2017</p>
                    </div>

                    <div className={resumeStyles.event}>
                        <p className={resumeStyles.underline}>ICAN (NGO) Committee Member</p>
                        <p>Part of the team responsible of promoting the NGO, to collect funds and organize visits to charities and shelter homes.
                </p>
                        <p>2017</p>
                    </div>

                    <div className={resumeStyles.publications}>
                        <h3>Publications <Emoji symbol="🔬" label="publication" /></h3>
                        <p className={resumeStyles.underline}>“An Overview of Ethical Concerns in Re-engineering the Human Body”</p>
                        <p>Scholar's Bulletin 10/17</p>
                        <p>DOI: 10.21276/sb.2017.3.10.16</p>
                    </div>

                    <div className={resumeStyles.awards}>
                        <h3>Awards <Emoji symbol="🏆" label="awards" /></h3>

                        <div className={resumeStyles.award}>
                            <p className={resumeStyles.underline}>Dean's List Award</p>
                            <p>Awarded to students who achieve GPA of 3.50  and above in a full academic semester. Recipient on multiple occasions.</p>
                        </div>

                        <div className={resumeStyles.award}>
                            <p className={resumeStyles.underline}>IEEE FYP Presentation Award</p>
                            <p>Awarded in recognition of presenting my final year project titled, "A Blockchain Solution to IoT-based Energy Security".
                    2nd Runner Up in the Computer Track</p>
                        </div>
                    </div>
                </div>
            </div>
        </Layout>
    )
}

export default ResumePage;
